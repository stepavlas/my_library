package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

@DisplayName("Language Validator should")
@ExtendWith(MockitoExtension::class)
class GenreValidatorTest {
    private lateinit var genreValidator: GenreValidator

    @BeforeEach
    internal fun setUp() {
        genreValidator = GenreValidator()
    }

    @Test
    @DisplayName("validate blank genre name when post")
    fun shouldValidatePostBlankGenreName() {
        val genre = Genre(1, "    ")

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            genreValidator.validateEntity(genre)
        }
    }

    @Test
    @DisplayName("validate null genre name when post")
    fun shouldValidatePostNullGenreName() {
        val genre = Genre(1, null)

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            genreValidator.validateEntity(genre)
        }
    }

}