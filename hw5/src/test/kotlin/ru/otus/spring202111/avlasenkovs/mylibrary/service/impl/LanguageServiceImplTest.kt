package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.LanguageDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.LanguageIntegrityCheckerImpl
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.LanguageValidator

@DisplayName("Language Service should")
@ExtendWith(MockitoExtension::class)
internal class LanguageServiceImplTest {
    @Mock
    private lateinit var languageDao: LanguageDao
    @Mock
    private lateinit var integrityChecker: LanguageIntegrityCheckerImpl
    @Mock
    private lateinit var validator: LanguageValidator

    @InjectMocks
    private lateinit var languageService: LanguageServiceImpl

    @Test
    @DisplayName("should return list of found languages")
    fun shouldReturnListOfFoundLanguages() {
        val languages = listOf(
            Language(1, "chinese"),
            Language(2, "spanish"),
            Language(3, "english")
        )

        Mockito.`when`(languageDao.findAll()).thenReturn(languages)

        val actual = languageService.listLanguages()
        assertIterableEquals(languages, actual)
    }

    @Test
    @DisplayName("should add language")
    fun shouldAddLanguage() {
        val newLanguage = Language(50, "turkish")
        Mockito.`when`(languageDao.insert(newLanguage)).thenReturn(newLanguage)

        val actual = languageService.addLanguage(newLanguage)

        verify(validator, times(1)).validateEntity(newLanguage)
        verify(integrityChecker, times(1)).checkBeforePost(newLanguage, null)

        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(newLanguage)
    }

    @Test
    @DisplayName("should update language")
    fun shouldUpdateLanguage() {
        val language = Language(2, "spanish")
        val expected = true

        Mockito.`when`(languageDao.update(language.id!!, language)).thenReturn(expected)

        val actual = languageService.updateLanguage(language.id!!, language)

        verify(validator, times(1)).validateEntity(language)
        verify(integrityChecker, times(1)).checkBeforePost(language, language.id)

        assertEquals(expected, actual)
    }

    @Test
    @DisplayName("should delete language")
    fun shouldDeleteLanguage() {
        val language = Language(2, "spanish")

        Mockito.`when`(languageDao.findById(language.id!!)).thenReturn(language)
        Mockito.`when`(languageDao.delete(language)).thenReturn(true)

        val actual = languageService.deleteLanguage(language.id!!)

        verify(integrityChecker, times(1)).checkBeforeDelete(language)
        
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(language)
    }

    @Test
    @DisplayName("should find language by id")
    fun shouldFindLanguageById() {
        val language = Language(2, "spanish")

        Mockito.`when`(languageDao.findById(language.id!!)).thenReturn(language)

        val actual = languageService.findById(language.id!!)
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(language)
    }
}