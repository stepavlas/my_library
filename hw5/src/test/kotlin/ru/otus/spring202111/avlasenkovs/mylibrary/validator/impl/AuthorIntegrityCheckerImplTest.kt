package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.AuthorDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import java.time.LocalDate

@DisplayName("Author Integrity Checker should")
@ExtendWith(MockitoExtension::class)
internal class AuthorIntegrityCheckerImplTest {

    @Mock
    private lateinit var authorDao: AuthorDao

    @Mock
    private lateinit var bookDao: BookDao

    @InjectMocks
    private lateinit var authorValidator: AuthorIntegrityCheckerImpl

    @Test
    @DisplayName("check author name already exists before post")
    fun shouldCheckBeforePostAuthorNameAlreadyExists() {
        val author = Author(
            firstName = "Alexander",
            lastName = "Pushkin",
            birthDate = LocalDate.of(1799, 6, 6),
            aboutLink = "https://en.wikipedia.org/wiki/Alexander_Pushkin"
        )

        Mockito.`when`(
            authorDao.existsByNameAndNotEqualsId(
                firstName = author.firstName!!,
                lastName = author.lastName!!,
                id = null
            )
        ).thenReturn(true)

        assertThrows(DataIntegrityViolationException::class.java) {
            authorValidator.checkBeforePost(
                author = author,
                id = null
            )
        }
    }

    @Test
    @DisplayName("pass check if everything correct before post")
    fun shouldPassCheckBeforePostAuthorOk() {
        val author = Author(
            firstName = "Ernest",
            lastName = "Hemingway",
            birthDate = LocalDate.of(1899, 7, 21),
            aboutLink = "https://en.wikipedia.org/wiki/Ernest_Hemingway"
        )

        Mockito.`when`(
            authorDao.existsByNameAndNotEqualsId(
                firstName = author.firstName!!,
                lastName = author.lastName!!,
                id = null
            )
        ).thenReturn(false)

        authorValidator.checkBeforePost(
            author = author,
            id = null
        )
    }



    @Test
    @DisplayName("check before delete author when used in book")
    fun shouldCheckBeforeDeleteAuthorUsedInBook() {
        val author = Author(
            firstName = "Ernest",
            lastName = "Hemingway",
            birthDate = LocalDate.of(1899, 7, 21),
            aboutLink = "abc"
        )

        Mockito.`when`(bookDao.existsByAuthor(author)).thenReturn(true)

        assertThrows(DataIntegrityViolationException::class.java) {
            authorValidator.checkBeforeDelete(author)
        }
    }

    @Test
    @DisplayName("pass check before delete author not used in book")
    fun shouldPassCheckBeforeDeleteAuthorNotUsedIdBook() {
        val author = Author(
            firstName = "Ernest",
            lastName = "Hemingway",
            birthDate = LocalDate.of(1899, 7, 21),
            aboutLink = "abc"
        )

        Mockito.`when`(bookDao.existsByAuthor(author)).thenReturn(false)

        authorValidator.checkBeforeDelete(author)
    }
}