package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.AuthorDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.AuthorIntegrityCheckerImpl
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.AuthorValidator
import java.time.LocalDate

@DisplayName("Author Service should")
@ExtendWith(MockitoExtension::class)
internal class AuthorServiceImplTest {
    @Mock
    private lateinit var authorDao: AuthorDao
    @Mock
    private lateinit var integrityChecker: AuthorIntegrityCheckerImpl
    @Mock
    private lateinit var validator: AuthorValidator

    @InjectMocks
    private lateinit var authorService: AuthorServiceImpl

    @Test
    @DisplayName("should return list of found authors")
    fun shouldReturnListOfFoundAuthors() {
        val authors = listOf(
            Author(
                id = 1,
                firstName = "Fyodor",
                lastName = "Dostoevsky",
                birthDate = LocalDate.of(1821, 11, 11),
                aboutLink = "https://en.wikipedia.org/wiki/Fyodor_Dostoevsky"
            ),
            Author(
                id = 2,
                firstName = "Mikhail",
                lastName = "Bulgakov",
                birthDate = LocalDate.of(1891, 5, 15),
                aboutLink = "https://en.wikipedia.org/wiki/Mikhail_Bulgakov"
            ),
            Author(
                id = 3,
                firstName = "Alexandre",
                lastName = "Dumas",
                birthDate = LocalDate.of(1802, 7, 24),
                aboutLink = "https://en.wikipedia.org/wiki/Alexandre_Dumas"
            )
        )

        Mockito.`when`(authorDao.findAll()).thenReturn(authors)

        val actual = authorService.listAuthors()
        assertIterableEquals(authors, actual)
    }

    @Test
    @DisplayName("should add author")
    fun shouldAddAuthor() {
        val newAuthor = Author(
            firstName = "Alexander",
            lastName = "Pushkin",
            birthDate = LocalDate.of(1799, 6, 6),
            aboutLink = "https://en.wikipedia.org/wiki/Alexander_Pushkin"
        )
        Mockito.`when`(authorDao.insert(newAuthor)).thenReturn(newAuthor)

        val actual = authorService.addAuthor(newAuthor)

        verify(integrityChecker, times(1)).checkBeforePost(newAuthor, null)

        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(newAuthor)
    }

    @Test
    @DisplayName("should update author")
    fun shouldUpdateAuthor() {
        val author = Author(
            id = 3,
            firstName = "Alexandre",
            lastName = "Dumas",
            birthDate = LocalDate.of(1802, 7, 24),
            aboutLink = "https://en.wikipedia.org/wiki/Alexandre_Dumas"
        )
        val expected = true

        Mockito.`when`(authorDao.update(author.id!!, author)).thenReturn(expected)

        val actual = authorService.updateAuthor(author.id!!, author)

        verify(validator, times(1)).validateEntity(author)
        verify(integrityChecker, times(1)).checkBeforePost(author, author.id)

        assertEquals(expected, actual)
    }

    @Test
    @DisplayName("should delete author")
    fun shouldDeleteAuthor() {
        val author =  Author(
            id = 3,
            firstName = "Alexandre",
            lastName = "Dumas",
            birthDate = LocalDate.of(1802, 7, 24),
            aboutLink = "https://en.wikipedia.org/wiki/Alexandre_Dumas"
        )

        Mockito.`when`(authorDao.findById(author.id!!)).thenReturn(author)
        Mockito.`when`(authorDao.delete(author)).thenReturn(true)

        val actual = authorService.deleteAuthor(author.id!!)

        verify(integrityChecker, times(1)).checkBeforeDelete(author)

        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(author)
    }

    @Test
    @DisplayName("should find author by id")
    fun shouldFindAuthorById() {
        val author =  Author(
            id = 3,
            firstName = "Alexandre",
            lastName = "Dumas",
            birthDate = LocalDate.of(1802, 7, 24),
            aboutLink = "https://en.wikipedia.org/wiki/Alexandre_Dumas"
        )

        Mockito.`when`(authorDao.findById(author.id!!)).thenReturn(author)

        val actual = authorService.findById(author.id!!)
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(author)
    }
}