package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.context.annotation.Import
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.GenreMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

@DisplayName("DAO for working with genres should")
@JdbcTest
@Import(GenreDaoJdbc::class, GenreMapper::class)
internal class GenreDaoJdbcIT {
    @Autowired
    private lateinit var genreDao: GenreDaoJdbc

    @Test
    @DisplayName("find all genres in table")
    fun shouldFindAllGenresInTable() {
        val genres = genreDao.findAll()
        assertEquals(5, genres.size)
        assertThat(genres).extracting("genre").containsOnly("Psychological Fiction", "Fantasy Fiction", "Satire", "Historical novel", "Realist novel")
    }

    @Test
    @DisplayName("find genre by id")
    fun shouldFindGenreById() {
        val searchId = 3L
        val genre = genreDao.findById(searchId)
        assertNotNull(genre)
        assertAll(
            { assertEquals(searchId, genre!!.id) },
            { assertEquals("Satire", genre!!.genre) }
        )
    }

    @Test
    @DisplayName("not find genre by id if not exists")
    fun shouldNotFindGenreByIdIfNotExists() {
        val genre = genreDao.findById(100)
        assertNull(genre)
    }

    @Test
    @DisplayName("insert new genre")
    fun shouldInsertNewGenre() {
        val genreValue = "Comedy"
        val newGenre = Genre(genre = genreValue)
        val insertedGenre = genreDao.insert(newGenre)

        assertNotNull(insertedGenre.id)
        val foundGenre = genreDao.findById(insertedGenre.id!!)

        assertNotNull(foundGenre)
        assertEquals(genreValue, foundGenre!!.genre)
    }

    @Test
    @DisplayName("update existing genre")
    fun shouldUpdateExistingGenre() {
        val originGenre = genreDao.findById(1)
        assertNotNull(originGenre)

        val newGenreValue = "Comedy"
        assertNotEquals(newGenreValue, originGenre!!.genre)
        val genre = Genre(genre = newGenreValue)

        val isUpdated = genreDao.update(originGenre.id!!, genre)
        assertTrue(isUpdated)

        val updatedGenre = genreDao.findById(originGenre.id!!)
        assertNotNull(updatedGenre)
        assertEquals(newGenreValue, updatedGenre!!.genre)
    }

    @Test
    @DisplayName("inform if genre not updated")
    fun shouldInformGenreNotUpdated() {
        val genre = Genre(genre = "Comedy")
        val isUpdated = genreDao.update(100, genre)
        assertFalse(isUpdated)
    }

    @Test
    @DisplayName("delete genre")
    fun shouldDeleteGenre() {
        val foundGenre = genreDao.findById(3)
        assertNotNull(foundGenre)

        val genre = Genre(foundGenre!!.id, "any_name")
        val isDeleted = genreDao.delete(genre)
        assertTrue(isDeleted)

        val foundGenre2 = genreDao.findById(foundGenre.id!!)
        assertTrue(foundGenre2 == null)
    }

    @Test
    @DisplayName("fail when deleting genre used in book")
    fun shouldDeleteFailGenreUsedInBook() {
        val foundGenre = genreDao.findById(2)
        assertNotNull(foundGenre)

        assertThrows(DataIntegrityViolationException::class.java) {
            genreDao.delete(foundGenre!!)
        }
    }

    @Test
    @DisplayName("fail when insert without genre specification")
    fun shouldInsertFailGenreNotSpecified() {
        val newGenre = Genre(genre = null)
        assertThrows(DataIntegrityViolationException::class.java) {
            genreDao.insert(newGenre)
        }
    }

    @Test
    @DisplayName("inform if genre with name already exists")
    fun shouldInformGenreByNameExists() {
        assertTrue(genreDao.existsByNameAndNotEqualsId(genreName = "FANTASY fiction", id = null))
    }

    @Test
    @DisplayName("inform if genre with name not exists")
    fun shouldInformGenreByNameNotExists() {
        assertFalse(genreDao.existsByNameAndNotEqualsId(genreName = "Comedy", id = null))
    }

    @Test
    @DisplayName("inform if genre with name not exists excluding genre with id")
    fun shouldInformGenreByNameNotExistsExcludingGenreWithId() {
        assertFalse(genreDao.existsByNameAndNotEqualsId(genreName = "FANTASY fiction", id = 2))
    }

    @Test
    @DisplayName("inform if genre with name already exists excluding genre with id")
    fun shouldInformGenreByNameExistsExcludingGenreWithId() {
        assertTrue(genreDao.existsByNameAndNotEqualsId(genreName = "FANTASY fiction", id = 3))
    }
}