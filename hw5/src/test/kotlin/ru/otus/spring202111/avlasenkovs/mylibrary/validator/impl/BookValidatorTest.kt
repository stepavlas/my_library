package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullAndEmptySource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.AuthorDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.GenreDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.LanguageDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import java.time.LocalDate
import java.time.Year

@DisplayName("Book Validator should")
@ExtendWith(MockitoExtension::class)
class BookValidatorTest {

    @Mock
    private lateinit var genreDao: GenreDao

    @Mock
    private lateinit var languageDao: LanguageDao

    @Mock
    private lateinit var authorDao: AuthorDao

    @InjectMocks
    private lateinit var bookValidator: BookValidator

    @ParameterizedTest
    @ValueSource(strings = ["A"])
    @NullAndEmptySource
    @DisplayName("validate null/blank book name")
    fun shouldValidatePostBlankBookName(name: String?) {
        val book = Book(
            name = name,
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 2, genre = "Fantasy Fiction"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), aboutLink = "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Call_of_the_Wild"
        )

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            bookValidator.validateEntity(book)
        }
    }

    @Test
    @DisplayName("validate book illegal url")
    fun shouldValidatePostBookIllegalUrl() {
        val book = Book(
            name = "The Call of the Wild",
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 2, genre = "Fantasy Fiction"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "abc"
        )

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            bookValidator.validateEntity(book)
        }
    }

    @Test
    @DisplayName("validate book genre not exists")
    fun shouldValidatePostBookGenreNotExists() {
        val book = Book(
            name = "The Call of the Wild",
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 100, genre = "Comedy"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Call_of_the_Wild"
        )

        validatePost(book)
    }

    @Test
    @DisplayName("pass validation when book is ok")
    fun shouldPassValidationBookOk() {
        val book = Book(
            name = "The Call of the Wild",
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 3, genre = "Satire"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Call_of_the_Wild"
        )
        stubFkEntities(book)

        bookValidator.validateEntity(book)
    }

    private fun validatePost(book: Book) {
        Assertions.assertThrows(DataIntegrityViolationException::class.java) {
            bookValidator.validateEntity(book)
        }
    }

    private fun stubFkEntities(book: Book) {
        Mockito.`when`(
            genreDao.findById(book.genre!!.id!!)
        ).thenReturn(book.genre)
        Mockito.`when`(
            languageDao.findById(book.language!!.id!!)
        ).thenReturn(book.language)
        Mockito.`when`(
            authorDao.findById(book.author!!.id!!)
        ).thenReturn(book.author)
    }

}