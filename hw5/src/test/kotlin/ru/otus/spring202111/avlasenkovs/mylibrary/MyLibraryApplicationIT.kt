package ru.otus.spring202111.avlasenkovs.mylibrary

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(properties = ["spring.shell.interactive.enabled=false"])
class MyLibraryApplicationIT {

    @Test
    fun contextLoads() {
    }

}
