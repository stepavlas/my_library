package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.BookIntegrityCheckerImpl
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.BookValidator
import java.time.LocalDate
import java.time.Year

@DisplayName("Book Service should")
@ExtendWith(MockitoExtension::class)
internal class BookServiceImplTest {
    @Mock
    private lateinit var bookDao: BookDao
    @Mock
    private lateinit var integrityChecker: BookIntegrityCheckerImpl
    @Mock
    private lateinit var validator: BookValidator

    @InjectMocks
    private lateinit var bookService: BookServiceImpl

    @Test
    @DisplayName("should return list of found books without search filter")
    fun shouldReturnListOfFoundBooksWithoutSearchFilter() {
        val books = createListOfBooks()

        Mockito.`when`(bookDao.findAll()).thenReturn(books)

        val actual = bookService.findBooks(null)
        assertIterableEquals(books, actual)
    }

    @Test
    @DisplayName("should return list of found books with search filter")
    fun shouldReturnListOfFoundBooksWithSearchFilter() {
        val books = createListOfBooks()

        val filtered = books.filterNot { it.id == 1L }

        Mockito.`when`(bookDao.findByName("The")).thenReturn(filtered)

        val actual = bookService.findBooks("The")
        assertIterableEquals(filtered, actual)
    }

    private fun createListOfBooks() = listOf(
        Book(
            id = 1,
            name = "Crime and Punishment",
            language = Language(id = 4, language = "russian"),
            year = Year.of(1866),
            genre = Genre(id = 1, genre = "Psychological Fiction"),
            author = Author(
                id = 1,
                firstName = "Fyodor",
                "Dostoevsky",
                LocalDate.of(1821, 11, 11),
                "https://en.wikipedia.org/wiki/Fyodor_Dostoevsky"
            ),
            aboutLink = "https://en.wikipedia.org/wiki/Crime_and_Punishment"
        ),
        createBook(),
        Book(
            id = 3,
            name = "The Three Musketeers",
            language = Language(id = 5, language = "french"),
            year = Year.of(1844),
            genre = Genre(id = 4, genre = "Historical novel"),
            author = Author(
                id = 3,
                firstName = "Alexandre",
                "Dumas",
                LocalDate.of(1802, 7, 24),
                "https://en.wikipedia.org/wiki/Alexandre_Dumas"
            ),
            aboutLink = "https://en.wikipedia.org/wiki/The_Three_Musketeers"
        )
    )

    @Test
    @DisplayName("should add book")
    fun shouldAddBook() {
        val newBook = Book(
            name = "The Call of the Wild",
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 2, genre = "Fantasy Fiction"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Call_of_the_Wild"
        )
        Mockito.`when`(bookDao.insert(newBook)).thenReturn(newBook)

        val actual = bookService.addBook(newBook)

        verify(validator, times(1)).validateEntity(newBook)
        verify(integrityChecker, times(1)).checkBeforePost(newBook, null)

        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(newBook)
    }

    @Test
    @DisplayName("should update book")
    fun shouldUpdateBook() {
        val book = createBook()
        val expected = true

        Mockito.`when`(bookDao.update(book.id!!, book)).thenReturn(expected)

        val actual = bookService.updateBook(book.id!!, book)

        verify(integrityChecker, times(1)).checkBeforePost(book, book.id)
        verify(validator, times(1)).validateEntity(book)

        assertEquals(expected, actual)
    }

    @Test
    @DisplayName("should delete book")
    fun shouldDeleteBook() {
        val book = createBook()

        Mockito.`when`(bookDao.findById(book.id!!)).thenReturn(book)
        Mockito.`when`(bookDao.delete(book)).thenReturn(true)

        val actual = bookService.deleteBook(book.id!!)
        
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(book)
    }

    @Test
    @DisplayName("should find book by id")
    fun shouldFindBookById() {
        val book = createBook()

        Mockito.`when`(bookDao.findById(book.id!!)).thenReturn(book)

        val actual = bookService.findById(book.id!!)
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(book)
    }

    private fun createBook() = Book(
        id = 2,
        name = "The Master and Margarita",
        language = Language(id = 4, language = "russian"),
        year = Year.of(1967),
        genre = Genre(id = 2, genre = "Fantasy Fiction"),
        author = Author(
            id = 2,
            firstName = "Mikhail",
            "Bulgakov",
            LocalDate.of(1891, 5, 15),
            "https://en.wikipedia.org/wiki/Mikhail_Bulgakov"
        ),
        aboutLink = "https://en.wikipedia.org/wiki/The_Master_and_Margarita"
    )
}