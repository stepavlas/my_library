package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import java.time.LocalDate

@DisplayName("Author Validator should")
@ExtendWith(MockitoExtension::class)
class AuthorValidatorTest {
    private lateinit var authorValidator: AuthorValidator

    @BeforeEach
    internal fun setUp() {
        authorValidator = AuthorValidator()
    }

    @Test
    @DisplayName("validate blank author name when post")
    fun shouldValidatePostBlankAuthorName() {
        val author = Author(
            firstName = "",
            lastName = "Pushkin",
            birthDate = LocalDate.of(1799, 6, 6),
            aboutLink = "https://en.wikipedia.org/wiki/Alexander_Pushkin"
        )

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            authorValidator.validateEntity(author)
        }
    }

    @Test
    @DisplayName("validate null author name when post")
    fun shouldValidatePostNullLanguageName() {
        val author = Author(
            firstName = "Alexander",
            lastName = "",
            birthDate = LocalDate.of(1799, 6, 6),
            aboutLink = "https://en.wikipedia.org/wiki/Alexander_Pushkin"
        )

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            authorValidator.validateEntity(author)
        }
    }

    @Test
    @DisplayName("validate author illegal url")
    fun shouldValidatePostAuthorIllegalUrl() {
        val author = Author(
            firstName = "Ernest",
            lastName = "Hemingway",
            birthDate = LocalDate.of(1899, 7, 21),
            aboutLink = "abc"
        )

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            authorValidator.validateEntity(author)
        }
    }

}