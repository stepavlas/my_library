package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.GenreDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.GenreIntegrityCheckerImpl
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.GenreValidator

@DisplayName("Genre Service should")
@ExtendWith(MockitoExtension::class)
internal class GenreServiceImplTest {
    @Mock
    private lateinit var genreDao: GenreDao
    @Mock
    private lateinit var integrityChecker: GenreIntegrityCheckerImpl
    @Mock
    private lateinit var validator: GenreValidator

    @InjectMocks
    private lateinit var genreService: GenreServiceImpl

    @Test
    @DisplayName("should return list of found genres")
    fun shouldReturnListOfFoundGenres() {
        val genres = listOf(
            Genre(1, "Psychological Fiction"),
            Genre(2, "Fantasy Fiction"),
            Genre(3, "Satire")
        )

        Mockito.`when`(genreDao.findAll()).thenReturn(genres)

        val actual = genreService.listGenres()
        assertIterableEquals(genres, actual)
    }

    @Test
    @DisplayName("should add genre")
    fun shouldAddGenre() {
        val newGenre = Genre(50, "Comedy")
        Mockito.`when`(genreDao.insert(newGenre)).thenReturn(newGenre)

        val actual = genreService.addGenre(newGenre)

        verify(integrityChecker, times(1)).checkBeforePost(newGenre, null)
        verify(validator, times(1)).validateEntity(newGenre)

        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(newGenre)
    }

    @Test
    @DisplayName("should update genre")
    fun shouldUpdateLanguage() {
        val genre = Genre(3, "Satire")
        val expected = true

        Mockito.`when`(genreDao.update(genre.id!!, genre)).thenReturn(expected)

        val actual = genreService.updateGenre(genre.id!!, genre)

        verify(integrityChecker, times(1)).checkBeforePost(genre, genre.id)
        verify(validator, times(1)).validateEntity(genre)

        assertEquals(expected, actual)
    }

    @Test
    @DisplayName("should delete genre")
    fun shouldDeleteGenre() {
        val genre = Genre(3, "Satire")

        Mockito.`when`(genreDao.findById(genre.id!!)).thenReturn(genre)
        Mockito.`when`(genreDao.delete(genre)).thenReturn(true)

        val actual = genreService.deleteGenre(genre.id!!)

        verify(integrityChecker, times(1)).checkBeforeDelete(genre)

        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(genre)
    }

    @Test
    @DisplayName("should find genre by id")
    fun shouldFindGenreById() {
        val genre = Genre(3, "Satire")

        Mockito.`when`(genreDao.findById(genre.id!!)).thenReturn(genre)

        val actual = genreService.findById(genre.id!!)
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(genre)
    }
}