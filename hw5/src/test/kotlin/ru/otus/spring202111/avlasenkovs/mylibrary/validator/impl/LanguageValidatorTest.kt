package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

@DisplayName("Language Validator should")
@ExtendWith(MockitoExtension::class)
class LanguageValidatorTest {

    private lateinit var languageValidator: LanguageValidator

    @BeforeEach
    internal fun setUp() {
        languageValidator = LanguageValidator()
    }

    @Test
    @DisplayName("validate blank language name when post")
    fun shouldValidatePostBlankLanguageName() {
        val language = Language(1, "    ")

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            languageValidator.validateEntity(language)
        }
    }

    @Test
    @DisplayName("validate null language name when post")
    fun shouldValidatePostNullLanguageName() {
        val language = Language(1, null)

        Assertions.assertThrows(IllegalArgumentException::class.java) {
            languageValidator.validateEntity(language)
        }
    }

}