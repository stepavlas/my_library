package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.context.annotation.Import
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.LanguageMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

@DisplayName("DAO for working with languages should")
@JdbcTest
@Import(LanguageDaoJdbc::class, LanguageMapper::class)
internal class LanguageDaoJdbcIT {
    @Autowired
    private lateinit var languageDao: LanguageDaoJdbc

    @Test
    @DisplayName("find all languages in table")
    fun shouldFindAllLanguagesInTable() {
        val languages = languageDao.findAll()
        assertEquals(5, languages.size)
        assertThat(languages).extracting("language").containsOnly("chinese", "spanish", "english", "russian", "french")
    }

    @Test
    @DisplayName("find language by id")
    fun shouldFindLanguageById() {
        val searchId = 2L
        val language = languageDao.findById(searchId)
        assertNotNull(language)
        assertAll(
            { assertEquals(searchId, language!!.id) },
            { assertEquals("spanish", language!!.language) }
        )
    }

    @Test
    @DisplayName("not find language by id if not exists")
    fun shouldNotFindLanguageByIdIfNotExists() {
        val language = languageDao.findById(100)
        assertNull(language)
    }

    @Test
    @DisplayName("insert new language")
    fun shouldInsertNewLanguage() {
        val languageValue = "portuguese"
        val newLanguage = Language(language = languageValue)
        val insertedLanguage = languageDao.insert(newLanguage)

        assertNotNull(insertedLanguage.id)
        val foundLanguage = languageDao.findById(insertedLanguage.id!!)

        assertNotNull(foundLanguage)
        assertEquals(languageValue, foundLanguage!!.language)
    }

    @Test
    @DisplayName("update existing language")
    fun shouldUpdateExistingLanguage() {
        val originLanguage = languageDao.findById(1)
        assertNotNull(originLanguage)

        val newLanguageValue = "portuguese"
        assertNotEquals(newLanguageValue, originLanguage!!.language)
        val language = Language(language = newLanguageValue)

        val isUpdated = languageDao.update(originLanguage.id!!, language)
        assertTrue(isUpdated)

        val updatedLanguage = languageDao.findById(originLanguage.id!!)
        assertNotNull(updatedLanguage)
        assertEquals(newLanguageValue, updatedLanguage!!.language)
    }

    @Test
    @DisplayName("inform if language not updated")
    fun shouldInformLanguageNotUpdated() {
        val language = Language(language = "portuguese")
        val isUpdated = languageDao.update(100, language)
        assertFalse(isUpdated)
    }

    @Test
    @DisplayName("delete languge")
    fun shouldDeleteLanguage() {
        val foundLanguage = languageDao.findById(2)
        assertNotNull(foundLanguage)

        val language = Language(foundLanguage!!.id, "any_name")
        val isDeleted = languageDao.delete(language)
        assertTrue(isDeleted)

        val foundLanguage2 = languageDao.findById(foundLanguage.id!!)
        assertTrue(foundLanguage2 == null)
    }

    @Test
    @DisplayName("fail when deleting language used in book")
    fun shouldDeleteFailLanguageUsedInBook() {
        val foundGenre = languageDao.findById(4)
        assertNotNull(foundGenre)

        assertThrows(DataIntegrityViolationException::class.java) {
            languageDao.delete(foundGenre!!)
        }
    }

    @Test
    @DisplayName("fail when insert without language specification")
    fun shouldInsertFailLanguageNotSpecified() {
        val newLanguage = Language(language = null)
        assertThrows(DataIntegrityViolationException::class.java) {
            languageDao.insert(newLanguage)
        }
    }

    @Test
    @DisplayName("inform if language with name already exists")
    fun shouldInformLanguageByNameExists() {
        assertTrue(languageDao.existsByNameAndNotEqualsId(languageName = "ENGLISH", id = null))
    }

    @Test
    @DisplayName("inform if language with name not exists")
    fun shouldInformLanguageByNameNotExists() {
        assertFalse(languageDao.existsByNameAndNotEqualsId(languageName = "portuguese", id = null))
    }

    @Test
    @DisplayName("inform if language with name not exists excluding genre with id")
    fun shouldInformLanguageByNameNotExistsExcludingLanguageWithId() {
        assertFalse(languageDao.existsByNameAndNotEqualsId(languageName = "ENGLISH", id = 3))
    }

    @Test
    @DisplayName("inform if language with name already exists excluding language with id")
    fun shouldInformLanguageByNameExistsExcludingLanguageWithId() {
        assertTrue(languageDao.existsByNameAndNotEqualsId(languageName = "english", id = 4))
    }


}