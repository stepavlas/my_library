package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.context.annotation.Import
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.AuthorMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.BookMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.GenreMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.LanguageMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import java.time.LocalDate
import java.time.Year

@DisplayName("DAO for working with books should")
@JdbcTest
@Import(BookDaoJdbc::class, BookMapper::class, LanguageMapper::class, GenreMapper::class, AuthorMapper::class)
internal class BookDaoJdbcIT {
    @Autowired
    private lateinit var bookDao: BookDaoJdbc

    @Test
    @DisplayName("find all books in table")
    fun shouldFindAllBooksInTable() {
        val books = bookDao.findAll()
        assertEquals(7, books.size)

        val actual = books.find { it.name == "The Three Musketeers" }
        assertNotNull(actual)

        val expected = Book(
            id = 3,
            name = "The Three Musketeers",
            language = Language(id = 5, language = "french"),
            year = Year.of(1844),
            genre = Genre(id = 4, genre = "Historical novel"),
            author = Author(id = 3, firstName = "Alexandre", "Dumas", LocalDate.of(1802, 7,24), "https://en.wikipedia.org/wiki/Alexandre_Dumas"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Three_Musketeers"
        )
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(expected)
    }

    @Test
    @DisplayName("find book by id")
    fun shouldFindBookById() {
        val searchId = 3L
        val actual = bookDao.findById(searchId)
        assertNotNull(actual)

        val expected = Book(
            id = 3,
            name = "The Three Musketeers",
            language = Language(id = 5, language = "french"),
            year = Year.of(1844),
            genre = Genre(id = 4, genre = "Historical novel"),
            author = Author(id = 3, firstName = "Alexandre", "Dumas", LocalDate.of(1802, 7,24), "https://en.wikipedia.org/wiki/Alexandre_Dumas"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Three_Musketeers"
        )
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(expected)
    }

    @Test
    @DisplayName("not find book by id if not exists")
    fun shouldNotFindBookByIdIfNotExists() {
        val book = bookDao.findById(100)
        assertNull(book)
    }

    @Test
    @DisplayName("insert new book")
    fun shouldInsertNewBook() {
        val newBook = Book(
            name = "The Call of the Wild",
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 2, genre = "Fantasy Fiction"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Call_of_the_Wild"
        )
        val insertedBook = bookDao.insert(newBook)

        assertNotNull(insertedBook.id)
        val foundBook = bookDao.findById(insertedBook.id!!)

        assertNotNull(foundBook)
        assertNotNull(foundBook!!.id)
        assertThat(foundBook)
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(newBook)
    }

    @Test
    @DisplayName("update existing book")
    fun shouldUpdateExistingBook() {
        val originBook = bookDao.findById(3)
        assertNotNull(originBook)

        val newYear = Year.of(1845)
        assertNotEquals(newYear, originBook!!.year)
        val newGenre = Genre(2, "Fantasy Fiction")
        assertNotEquals(newGenre, originBook.genre)
        val newAboutLink = "https://The_Three_Musketeers.com"
        assertNotEquals(newAboutLink, originBook.aboutLink)

        val book = Book(
            id = originBook.id,
            name = originBook.name,
            language = originBook.language,
            year = newYear,
            genre = newGenre,
            author = originBook.author,
            aboutLink = newAboutLink
        )

        val isUpdated = bookDao.update(originBook.id!!, book)
        assertTrue(isUpdated)

        val updatedBook = bookDao.findById(originBook.id!!)
        assertNotNull(updatedBook)
        assertEquals(originBook.id, updatedBook!!.id)
        assertThat(updatedBook)
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(book)
    }

    @Test
    @DisplayName("inform if book not updated")
    fun shouldInformBookNotUpdated() {
        val book = Book(
            name = "The Three Musketeers",
            language = Language(id = 5, language = "french"),
            year = Year.of(1845),
            genre = Genre(2, "Fantasy Fiction"),
            author = Author(id = 3, firstName = "Alexandre", "Dumas", LocalDate.of(1802, 7,24), "https://en.wikipedia.org/wiki/Alexandre_Dumas"),
            aboutLink = "https://The_Three_Musketeers.com"
        )

        val isUpdated = bookDao.update(100, book)
        assertFalse(isUpdated)
    }

    @Test
    @DisplayName("delete Book")
    fun shouldDeleteBook() {
        val foundBook = bookDao.findById(3)
        assertNotNull(foundBook)

        val book = Book(
            id = 3,
            name = "The Three Musketeers",
            language = Language(id = 5, language = "french"),
            year = Year.of(1844),
            genre = Genre(id = 4, genre = "Historical novel"),
            author = Author(id = 3, firstName = "Alexandre", "Dumas", LocalDate.of(1802, 7,24), "https://en.wikipedia.org/wiki/Alexandre_Dumas"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Three_Musketeers"
        )
        val isDeleted = bookDao.delete(book)
        assertTrue(isDeleted)

        val foundBook2 = bookDao.findById(book.id!!)
        assertTrue(foundBook2 == null)
    }

    @Test
    @DisplayName("fail when insert with name is null")
    fun shouldInsertFailNameIsNull() {
        val newBook = Book(
            name = null,
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 2, genre = "Fantasy Fiction"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Call_of_the_Wild"
        )

        assertThrows(DataIntegrityViolationException::class.java) {
            bookDao.insert(newBook)
        }
    }

    @Test
    @DisplayName("inform that book with genre exists")
    fun shouldInformBookExistByGenre() {
        val genre = Genre(1, "Psychological Fiction")
        assertTrue(bookDao.existsByGenre(genre))
    }

    @Test
    @DisplayName("inform that book with genre not exists")
    fun shouldInformBookNotExistByGenre() {
        val genre = Genre(100, "Science fiction")
        assertFalse(bookDao.existsByGenre(genre))
    }

    @Test
    @DisplayName("inform that book with language exists")
    fun shouldInformBookExistByLanguage() {
        val language = Language(5, "french")
        assertTrue(bookDao.existsByLanguage(language))
    }

    @Test
    @DisplayName("inform that book with language not exists")
    fun shouldInformBookNotExistByLanguage() {
        val language = Language(100, "italian")
        assertFalse(bookDao.existsByLanguage(language))
    }

    @Test
    @DisplayName("inform that book with author exists")
    fun shouldInformBookExistByAuthor() {
        val author = Author(
            id = 4,
            firstName = "Victor",
            lastName = "Hugo",
            birthDate = LocalDate.of(1802, 2, 26),
            aboutLink = "https://en.wikipedia.org/wiki/Victor_Hugo"
        )
        assertTrue(bookDao.existsByAuthor(author))
    }

    @Test
    @DisplayName("inform that book with author not exists")
    fun shouldInformBookNotExistByAuthor() {
        val author = Author(
            id = 100,
            firstName = "Alexander",
            lastName = "Pushkin",
            birthDate = LocalDate.of(1799, 6, 6),
            aboutLink = "https://en.wikipedia.org/wiki/Alexander_Pushkin"
        )
        assertFalse(bookDao.existsByAuthor(author))
    }

    @Test
    @DisplayName("inform if book with name already exists")
    fun shouldInformBookByNameExists() {
        assertTrue(bookDao.existsByNameAndNotEqualsId(name = "The Master and Margarita", id = null))
    }

    @Test
    @DisplayName("inform if book with name not exists")
    fun shouldInformBookByNameNotExists() {
        assertFalse(bookDao.existsByNameAndNotEqualsId(name = "The Call of the Wild", id = null))
    }

    @Test
    @DisplayName("inform if book with name not exists excluding book with id")
    fun shouldInformBookByNameNotExistsExcludingBookWithId() {
        assertFalse(bookDao.existsByNameAndNotEqualsId(name = "The Three Musketeers", id = 3))
    }

    @Test
    @DisplayName("inform if book with name already exists excluding book with id")
    fun shouldInformBookByNameExistsExcludingBookWithId() {
        assertTrue(bookDao.existsByNameAndNotEqualsId(name = "The Three Musketeers", id = 2))
    }

    @Test
    @DisplayName("find book by name")
    fun shouldFindBookByName() {
        val actualCollection = bookDao.findByName("monte cristo")
        assertEquals(1, actualCollection.size)

        val actual = actualCollection.first()
        val expected = Book(
            id = 4,
            name = "The Count of Monte Cristo",
            language = Language(id = 5, language = "french"),
            year = Year.of(1844),
            genre = Genre(id = 4, genre = "Historical novel"),
            author = Author(id = 3, firstName = "Alexandre", "Dumas", LocalDate.of(1802, 7,24), "https://en.wikipedia.org/wiki/Alexandre_Dumas"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Count_of_Monte_Cristo"
        )
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(expected)
    }


}