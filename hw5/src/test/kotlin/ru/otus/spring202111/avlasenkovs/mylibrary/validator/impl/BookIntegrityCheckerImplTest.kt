package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import java.time.LocalDate
import java.time.Year

@DisplayName("Book Integrity Checker should")
@ExtendWith(MockitoExtension::class)
internal class BookIntegrityCheckerImplTest {
    @Mock
    private lateinit var bookDao: BookDao

    @InjectMocks
    private lateinit var bookValidator: BookIntegrityCheckerImpl

    @Test
    @DisplayName("check book name already exists before post")
    fun shouldCheckBeforePostBookNameAlreadyExists() {
        val book = Book(
            name = "The Three Musketeers",
            language = Language(id = 5, language = "french"),
            year = Year.of(1844),
            genre = Genre(id = 4, genre = "Historical novel"),
            author = Author(id = 3, firstName = "Alexandre", "Dumas", LocalDate.of(1802, 7,24), aboutLink = "https://en.wikipedia.org/wiki/Alexandre_Dumas"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Three_Musketeers"
        )

        Mockito.`when`(
            bookDao.existsByNameAndNotEqualsId(
                name = book.name!!,
                id = null
            )
        ).thenReturn(true)

        assertThrows(DataIntegrityViolationException::class.java) {
            bookValidator.checkBeforePost(
                book = book,
                id = null
            )
        }
    }

    @Test
    @DisplayName("pass check if everything correct before post")
    fun shouldPassCheckBeforePostBookOk() {
        val book = Book(
            name = "The Call of the Wild",
            language = Language(id = 3, language = "english"),
            year = Year.of(1903),
            genre = Genre(id = 2, genre = "Fantasy Fiction"),
            author = Author(id = 5, firstName = "Jack", "London", LocalDate.of(1876, 1,12), "https://en.wikipedia.org/wiki/Jack_London"),
            aboutLink = "https://en.wikipedia.org/wiki/The_Call_of_the_Wild"
        )

        Mockito.`when`(
            bookDao.existsByNameAndNotEqualsId(
                name = book.name!!,
                id = null
            )
        ).thenReturn(false)

        bookValidator.checkBeforePost(
            book = book,
            id = null
        )
    }
}