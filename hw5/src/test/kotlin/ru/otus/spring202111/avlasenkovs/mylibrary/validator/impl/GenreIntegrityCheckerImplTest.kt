package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.GenreDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

@DisplayName("Genre Integrity Checker should")
@ExtendWith(MockitoExtension::class)
internal class GenreIntegrityCheckerImplTest {

    @Mock
    private lateinit var genreDao: GenreDao

    @Mock
    private lateinit var bookDao: BookDao

    @InjectMocks
    private lateinit var integrityChecker: GenreIntegrityCheckerImpl

    @Test
    @DisplayName("check genre name already exists before post")
    fun shouldCheckBeforePostGenreAlreadyExists() {
        val genre = Genre(3, "Satire")

        Mockito.`when`(
            genreDao.existsByNameAndNotEqualsId(
                genreName = genre.genre!!,
                id = null
            )
        ).thenReturn(true)

        assertThrows(DataIntegrityViolationException::class.java) {
            integrityChecker.checkBeforePost(
                genre = genre,
                id = null
            )
        }
    }

    @Test
    @DisplayName("pass check if everything correct before post")
    fun shouldPassCheckBeforePostGenreOk() {
        val genre = Genre(6, "Fairy tale")

        Mockito.`when`(
            genreDao.existsByNameAndNotEqualsId(
                genreName = genre.genre!!,
                id = null
            )
        ).thenReturn(false)

        integrityChecker.checkBeforePost(
            genre = genre,
            id = null
        )
    }

    @Test
    @DisplayName("check before delete genre when used in book")
    fun shouldCheckBeforeDeleteGenreUsedInBook() {
        val genre = Genre(3, "Satire")

        Mockito.`when`(bookDao.existsByGenre(genre)).thenReturn(true)

        assertThrows(DataIntegrityViolationException::class.java) {
            integrityChecker.checkBeforeDelete(genre)
        }
    }

    @Test
    @DisplayName("pass check before delete genre not used in book")
    fun shouldPassCheckBeforeDeleteGenreNotUsedIdBook() {
        val genre = Genre(3, "Satire")

        Mockito.`when`(bookDao.existsByGenre(genre)).thenReturn(false)

        integrityChecker.checkBeforeDelete(genre)
    }
}