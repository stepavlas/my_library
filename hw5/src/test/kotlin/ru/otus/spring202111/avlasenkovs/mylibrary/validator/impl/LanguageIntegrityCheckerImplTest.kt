package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.LanguageDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

@DisplayName("Language Integrity Checker should")
@ExtendWith(MockitoExtension::class)
internal class LanguageIntegrityCheckerImplTest {

    @Mock
    private lateinit var languageDao: LanguageDao

    @Mock
    private lateinit var bookDao: BookDao

    @InjectMocks
    private lateinit var integrityChecker: LanguageIntegrityCheckerImpl



    @Test
    @DisplayName("check language name already exists before post")
    fun shouldCheckBeforePostLanguageAlreadyExists() {
        val language = Language(3, "spanish")

        Mockito.`when`(
            languageDao.existsByNameAndNotEqualsId(
                languageName = language.language!!,
                id = null
            )
        ).thenReturn(true)

        assertThrows(DataIntegrityViolationException::class.java) {
            integrityChecker.checkBeforePost(
                language = language,
                id = null
            )
        }
    }

    @Test
    @DisplayName("pass check if everything correct before post")
    fun shouldPassCheckBeforePostLanguageOk() {
        val language = Language(6, "turkish")

        Mockito.`when`(
            languageDao.existsByNameAndNotEqualsId(
                languageName = language.language!!,
                id = null
            )
        ).thenReturn(false)

        integrityChecker.checkBeforePost(
            language = language,
            id = null
        )
    }

    @Test
    @DisplayName("check before delete language when used in book")
    fun shouldCheckBeforeDeleteLanguageUsedInBook() {
        val language = Language(3, "spanish")

        Mockito.`when`(bookDao.existsByLanguage(language)).thenReturn(true)

        assertThrows(DataIntegrityViolationException::class.java) {
            integrityChecker.checkBeforeDelete(language)
        }
    }

    @Test
    @DisplayName("pass check before delete language not used in book")
    fun shouldPassCheckBeforeDeleteLanguageNotUsedInBook() {
        val language = Language(3, "spanish")

        Mockito.`when`(bookDao.existsByLanguage(language)).thenReturn(false)

        integrityChecker.checkBeforeDelete(language)
    }
}