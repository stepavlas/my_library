package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest
import org.springframework.context.annotation.Import
import org.springframework.dao.DataIntegrityViolationException
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.AuthorMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import java.time.LocalDate

@DisplayName("DAO for working with authors should")
@JdbcTest
@Import(AuthorDaoJdbc::class, AuthorMapper::class)
internal class AuthorDaoJdbcIT {
    @Autowired
    private lateinit var authorDao: AuthorDaoJdbc

    @Test
    @DisplayName("find all authors in table")
    fun shouldFindAllAuthorsInTable() {
        val authors = authorDao.findAll()
        assertEquals(5, authors.size)

        val actual = authors.find { it.lastName == "Dostoevsky" }
        assertNotNull(actual)

        val expected = Author(
            firstName = "Fyodor",
            lastName = "Dostoevsky",
            birthDate = LocalDate.of(1821, 11, 11),
            aboutLink = "https://en.wikipedia.org/wiki/Fyodor_Dostoevsky"
        )
        assertThat(actual).usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(expected)
    }

    @Test
    @DisplayName("find author by id")
    fun shouldFindAuthorById() {
        val searchId = 4L
        val actual = authorDao.findById(searchId)
        assertNotNull(actual)

        val expected = Author(
            id = 4,
            firstName = "Victor",
            lastName = "Hugo",
            birthDate = LocalDate.of(1802, 2, 26),
            aboutLink = "https://en.wikipedia.org/wiki/Victor_Hugo"
        )
        assertThat(actual).usingRecursiveComparison()
            .isEqualTo(expected)
    }

    @Test
    @DisplayName("not find author by id if not exists")
    fun shouldNotFindAuthorByIdIfNotExists() {
        val author = authorDao.findById(100)
        assertNull(author)
    }

    @Test
    @DisplayName("insert new author")
    fun shouldInsertNewAuthor() {
        val newAuthor = Author(
            id = 4,
            firstName = "Alexander",
            lastName = "Pushkin",
            birthDate = LocalDate.of(1799, 6, 6),
            aboutLink = "https://en.wikipedia.org/wiki/Alexander_Pushkin"
        )
        val insertedAuthor = authorDao.insert(newAuthor)

        assertNotNull(insertedAuthor.id)
        val foundAuthor = authorDao.findById(insertedAuthor.id!!)

        assertNotNull(foundAuthor)
        assertNotEquals(newAuthor.id, foundAuthor!!.id)
        assertThat(foundAuthor)
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(newAuthor)
    }

    @Test
    @DisplayName("update existing author")
    fun shouldUpdateExistingAuthor() {
        val originAuthor = authorDao.findById(3)
        assertNotNull(originAuthor)

        val newFirstName = "Pavel"
        assertNotEquals(newFirstName, originAuthor!!.firstName)
        val newAboutLink = "https://en.wikipedia.org/wiki/Pavel_Dumas"
        assertNotEquals(newAboutLink, originAuthor.aboutLink)

        val author = Author(
            firstName = "Pavel",
            lastName = originAuthor.lastName,
            birthDate = originAuthor.birthDate,
            aboutLink = newAboutLink
        )

        val isUpdated = authorDao.update(originAuthor.id!!, author)
        assertTrue(isUpdated)

        val updatedAuthor = authorDao.findById(originAuthor.id!!)
        assertNotNull(updatedAuthor)
        assertEquals(originAuthor.id, updatedAuthor!!.id)
        assertThat(updatedAuthor)
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(author)
    }

    @Test
    @DisplayName("inform if author not updated")
    fun shouldInformAuthorNotUpdated() {
        val author = Author(
            firstName = "Pavel",
            lastName = "Dumas",
            birthDate = LocalDate.of(1802, 7, 24),
            aboutLink = "https://en.wikipedia.org/wiki/Pavel_Dumas"
        )

        val isUpdated = authorDao.update(100, author)
        assertFalse(isUpdated)
    }

    @Test
    @DisplayName("delete Author")
    fun shouldDeleteAuthor() {
        val author = authorDao.findById(5)
        assertNotNull(author)

        val isDeleted = authorDao.delete(author!!)
        assertTrue(isDeleted)

        val foundAuthor2 = authorDao.findById(author.id!!)
        assertTrue(foundAuthor2 == null)
    }

    @Test
    @DisplayName("fail when deleting author used in book")
    fun shouldDeleteFailAuthorUsedInBook() {
        val author = authorDao.findById(4)
        assertNotNull(author)

        assertThrows(DataIntegrityViolationException::class.java) {
            authorDao.delete(author!!)
        }
    }

    @Test
    @DisplayName("fail when insert without mandatory field specification")
    fun shouldInsertFailMandatoryFieldNotSpecified() {
        val newAuthor = Author(
            firstName = null,
            lastName = "Pushkin",
            birthDate = LocalDate.of(1799, 6, 6),
            aboutLink = "https://en.wikipedia.org/wiki/Alexander_Pushkin"
        )
        assertThrows(DataIntegrityViolationException::class.java) {
            authorDao.insert(newAuthor)
        }
    }

    @Test
    @DisplayName("insert only mandatory fields specified")
    fun shouldInsertOnlyMandatoryFieldsSpecified() {
        val newAuthor = Author(
            firstName = "Alexander",
            lastName = "Pushkin",
            birthDate = null,
            aboutLink = null
        )
        val insertedAuthor = authorDao.insert(newAuthor)

        assertNotNull(insertedAuthor.id)
        val foundAuthor = authorDao.findById(insertedAuthor.id!!)

        assertNotNull(foundAuthor)
        assertNotEquals(newAuthor.id, foundAuthor!!.id)
        assertThat(foundAuthor)
            .usingRecursiveComparison()
            .ignoringFields("id")
            .isEqualTo(newAuthor)
    }

    @Test
    @DisplayName("inform if author with name already exists")
    fun shouldInformAuthorByNameExists() {
        assertTrue(authorDao.existsByNameAndNotEqualsId(firstName = "Mikhail", lastName = "Bulgakov", id = null))
    }

    @Test
    @DisplayName("inform if author with name not exists")
    fun shouldInformAuthorByNameNotExists() {
        assertFalse(authorDao.existsByNameAndNotEqualsId(firstName = "Leo", lastName = "Tolstoy", id = null))
    }

    @Test
    @DisplayName("inform if author with name not exists excluding author with id")
    fun shouldInformAuthorByNameNotExistsExcludingAuthorWithId() {
        assertFalse(authorDao.existsByNameAndNotEqualsId(firstName = "Mikhail", lastName = "Bulgakov", id = 2))
    }

    @Test
    @DisplayName("inform if author with name already exists excluding author with id")
    fun shouldInformAuthorByNameExistsExcludingAuthorWithId() {
        assertTrue(authorDao.existsByNameAndNotEqualsId(firstName = "Mikhail", lastName = "Bulgakov", id = 3))
    }
}