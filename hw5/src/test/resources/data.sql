INSERT INTO genre
(genre)
VALUES
('Psychological Fiction'),
('Fantasy Fiction'),
('Satire'),
('Historical novel'),
('Realist novel');

INSERT INTO lang
(lang)
VALUES
('chinese'),
('spanish'),
('english'),
('russian'),
('french');

INSERT INTO author
(first_name, last_name, birth_date, about_link)
VALUES
('Fyodor', 'Dostoevsky', '1821-11-11', 'https://en.wikipedia.org/wiki/Fyodor_Dostoevsky'),
('Mikhail', 'Bulgakov', '1891-05-15', 'https://en.wikipedia.org/wiki/Mikhail_Bulgakov'),
('Alexandre', 'Dumas', '1802-07-24', 'https://en.wikipedia.org/wiki/Alexandre_Dumas'),
('Victor', 'Hugo', '1802-02-26', 'https://en.wikipedia.org/wiki/Victor_Hugo'),
('Jack', 'London', '1876-01-12', 'https://en.wikipedia.org/wiki/Jack_London');

INSERT INTO book
(name, lang_fk, year, genre_fk, author_fk, about_link)
VALUES
('Crime and Punishment', 4, 1866, 1, 1, 'https://en.wikipedia.org/wiki/Crime_and_Punishment'),
('The Master and Margarita', 4, 1967, 2, 2, 'https://en.wikipedia.org/wiki/The_Master_and_Margarita'),
('The Three Musketeers', 5, 1844, 4, 3, 'https://en.wikipedia.org/wiki/The_Three_Musketeers'),
('The Count of Monte Cristo', 5, 1844, 4, 3, 'https://en.wikipedia.org/wiki/The_Count_of_Monte_Cristo'),
('Les Misérables', 5, 1862, 4, 4, 'https://en.wikipedia.org/wiki/Les_Mis%C3%A9rables'),
('The Hunchback of Notre Dame', 5, 1831, 1, 4, 'https://en.wikipedia.org/wiki/The_Hunchback_of_Notre-Dame'),
('The Tale of Tsar Saltan', 4, 1831, 2, 4, 'https://en.wikipedia.org/wiki/The_Tale_of_Tsar_Saltan');