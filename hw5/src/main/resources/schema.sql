CREATE TABLE author (
	id BIGSERIAL NOT NULL,
	first_name VARCHAR(30) NOT NULL,
	last_name VARCHAR(30) NOT NULL,
	birth_date DATE,
	about_link VARCHAR (255),
	unique (first_name, last_name),
	CONSTRAINT author_id_pk PRIMARY KEY (id)
);

CREATE TABLE lang (
	id SERIAL NOT NULL,
	lang VARCHAR(30) NOT NULL UNIQUE,
	CONSTRAINT lang_id_pk PRIMARY KEY (id)
);

CREATE TABLE genre (
    id SERIAL NOT NULL,
	genre VARCHAR(30) NOT NULL UNIQUE,
	CONSTRAINT genre_id_pk PRIMARY KEY (id)
);

CREATE TABLE book (
    id BIGSERIAL NOT NULL,
	name VARCHAR(300) NOT NULL UNIQUE,
	lang_fk INTEGER REFERENCES lang (id),
	year INTEGER CHECK (year > 0),
	genre_fk INTEGER REFERENCES genre (id),
	author_fk INTEGER REFERENCES author (id),
	about_link VARCHAR(255),
	CONSTRAINT book_id_pk PRIMARY KEY (id)
);