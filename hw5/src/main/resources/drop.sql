drop table if exists book cascade;
drop table if exists genre cascade;
drop table if exists lang cascade;
drop table if exists author cascade;