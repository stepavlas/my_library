package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.GenreDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.GenreIntegrityChecker

@Component
class GenreIntegrityCheckerImpl(
    private val genreDao: GenreDao,
    private val bookDao: BookDao
    ) : GenreIntegrityChecker {

    override fun checkBeforePost(genre: Genre, id: Long?) {
        if (genreDao.existsByNameAndNotEqualsId(genreName = genre.genre!!, id = id)) {
            throw DataIntegrityViolationException("Genre with name [${genre.genre}] already exists")
        }
    }

    override fun checkBeforeDelete(entity: Genre) {
        if (bookDao.existsByGenre(entity)) {
            throw DataIntegrityViolationException("Can't delete genre [${entity} used in book")
        }
    }
}