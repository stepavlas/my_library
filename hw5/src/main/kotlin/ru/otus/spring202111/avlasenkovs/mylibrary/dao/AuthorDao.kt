package ru.otus.spring202111.avlasenkovs.mylibrary.dao

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author

interface AuthorDao {

    fun findAll(): Collection<Author>

    fun findById(id: Long): Author?

    fun insert(author: Author): Author

    fun update(id: Long, author: Author): Boolean

    fun delete(author: Author): Boolean

    fun existsByNameAndNotEqualsId(firstName: String, lastName: String, id: Long?): Boolean
}