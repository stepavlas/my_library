package ru.otus.spring202111.avlasenkovs.mylibrary.validator

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book

interface BookIntegrityChecker {
    fun checkBeforePost(book: Book, id: Long?)
}