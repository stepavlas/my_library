package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper

import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import java.sql.ResultSet

@Component
class GenreMapper : RowMapper<Genre> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Genre? {
        val id = rs.getLong("genre_id")
        return if (id != 0L) {
            val genre = rs.getString("genre_genre")
            Genre(id, genre)
        } else null
    }
}