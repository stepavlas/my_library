package ru.otus.spring202111.avlasenkovs.mylibrary.domain

class Language(
    val id: Long? = null,
    val language: String? = null
) {
    override fun toString(): String {
        return "Language(id=$id, language=$language)"
    }
}