package ru.otus.spring202111.avlasenkovs.mylibrary.validator

interface Validator<E> {
    fun validateEntity(entity: E)
}