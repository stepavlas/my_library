package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.LanguageDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.service.LanguageService
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.LanguageIntegrityChecker
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.LanguageValidator

@Service
class LanguageServiceImpl(
    private val languageDao: LanguageDao,
    private val integrityChecker: LanguageIntegrityChecker,
    private val validator: LanguageValidator
    ) : LanguageService {
    override fun listLanguages(): Collection<Language> = languageDao.findAll()

    override fun addLanguage(language: Language): Language {
        validator.validateEntity(language)
        integrityChecker.checkBeforePost(
            language = language,
            id = null
        )

        if (languageDao.existsByNameAndNotEqualsId(language.language!!, null)) {
            throw DataIntegrityViolationException("Language with name [${language.language}] already exists")
        }

        return languageDao.insert(language)
    }

    override fun updateLanguage(id: Long, language: Language): Boolean {
        validator.validateEntity(language)
        integrityChecker.checkBeforePost(
            language = language,
            id = id
        )

        return languageDao.update(id, language)
    }

    override fun deleteLanguage(id: Long): Language? {
        val language = languageDao.findById(id) ?: return null
        integrityChecker.checkBeforeDelete(language)

        val isDeleted = languageDao.delete(language)
        return if (isDeleted) language else null
    }

    override fun findById(id: Long) = languageDao.findById(id)
}