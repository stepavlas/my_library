package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.springframework.stereotype.Service
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.GenreDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.service.GenreService
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.GenreIntegrityChecker
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl.GenreValidator

@Service
class GenreServiceImpl(
    private val genreDao: GenreDao,
    private val validator: GenreValidator,
    private val entityChecker: GenreIntegrityChecker
    ) : GenreService {
    override fun listGenres() = genreDao.findAll()

    override fun addGenre(genre: Genre): Genre {
        validator.validateEntity(genre)
        entityChecker.checkBeforePost(
            genre = genre,
            id = null
        )

        return genreDao.insert(genre)
    }

    override fun updateGenre(id: Long, genre: Genre): Boolean {
        validator.validateEntity(genre)
        entityChecker.checkBeforePost(genre, id)

        return genreDao.update(id, genre)
    }

    override fun deleteGenre(id: Long): Genre? {
        val genre = genreDao.findById(id) ?: return null
        entityChecker.checkBeforeDelete(genre)

        val isDeleted = genreDao.delete(genre)
        return if (isDeleted) genre else null
    }

    override fun findById(genreId: Long) = genreDao.findById(genreId)
}