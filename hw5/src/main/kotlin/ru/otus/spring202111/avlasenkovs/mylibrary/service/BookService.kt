package ru.otus.spring202111.avlasenkovs.mylibrary.service

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book

interface BookService {
    fun findBooks(searchStr: String?): List<Book>

    fun findById(id: Long): Book?

    fun addBook(book: Book): Book

    fun updateBook(id: Long, book: Book): Boolean

    fun deleteBook(id: Long): Book?
}