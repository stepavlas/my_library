package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.AuthorDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.GenreDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.LanguageDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.util.CommonRegex
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.Validator
import java.lang.IllegalArgumentException

@Component
class BookValidator(
    private val genreDao: GenreDao,
    private val languageDao: LanguageDao,
    private val authorDao: AuthorDao
) : Validator<Book> {
    override fun validateEntity(entity: Book) {
        if (entity.name.isNullOrBlank() || entity.name.length < 2) {
            throw IllegalArgumentException("Trying to insert book with null/blank/incorrect name value")
        }
        if (entity.aboutLink != null && !entity.aboutLink.matches(CommonRegex.URL_REGEX)) {
            throw IllegalArgumentException("Trying to insert book with incorrect url value")
        }

        validateFks(entity)
    }

    private fun validateFks(book: Book) {
        book.genre?.let {
            genreDao.findById(it.id!!) ?: throw DataIntegrityViolationException("Genre [$it] not found")
        }
        book.language?.let {
            languageDao.findById(it.id!!) ?: throw DataIntegrityViolationException("Language [$it] not found")
        }
        book.author?.let {
            authorDao.findById(it.id!!) ?: throw DataIntegrityViolationException("Author [$it] not found")
        }
    }
}