package ru.otus.spring202111.avlasenkovs.mylibrary.validator

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

interface GenreIntegrityChecker {

    fun checkBeforePost(genre: Genre, id: Long?)

    fun checkBeforeDelete(entity: Genre)

}