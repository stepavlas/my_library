package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.Validator
import java.lang.IllegalArgumentException

@Component
class LanguageValidator: Validator<Language> {
    override fun validateEntity(entity: Language) {
        if (entity.language.isNullOrBlank()) {
            throw IllegalArgumentException("Trying to insert language with null or blank language value")
        }
    }
}