package ru.otus.spring202111.avlasenkovs.mylibrary

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MyLibraryApplication

fun main(args: Array<String>) {
    runApplication<MyLibraryApplication>(*args)
}
