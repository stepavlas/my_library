package ru.otus.spring202111.avlasenkovs.mylibrary.service

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

interface LanguageService {

    fun listLanguages(): Collection<Language>

    fun addLanguage(language: Language): Language

    fun updateLanguage(id: Long, language: Language): Boolean

    fun deleteLanguage(id: Long): Language?

    fun findById(id: Long): Language?
}
