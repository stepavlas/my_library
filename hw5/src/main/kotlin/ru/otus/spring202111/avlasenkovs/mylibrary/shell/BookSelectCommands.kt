package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.service.BookService

@ShellComponent
class BookSelectCommands(
    private val bookService: BookService
    ) {

    @ShellMethod(value = "get list of books with optional filter", key = ["list-b"])
    fun list(@ShellOption(defaultValue = "") filter: String?): List<String> {
        val searchStr = if (filter == "") null else filter
        return bookService.findBooks(searchStr).map { "${it.id}: ${it.name}" }
    }

    @ShellMethod(value = "get book data", key = ["get-b"])
    fun get(@ShellOption id: Long): String {
        val foundBook = bookService.findById(id)
        return if (foundBook != null) {
            "id = [${foundBook.id}], name = [${foundBook.name}], language = [${foundBook.language}], " +
                "year = [${foundBook.year}], genre = [${foundBook.genre}], author = [${foundBook.author}], " +
                "aboutLink = [${foundBook.aboutLink}]"
        } else {
            "Book with id [$id] not found"
        }
    }
}