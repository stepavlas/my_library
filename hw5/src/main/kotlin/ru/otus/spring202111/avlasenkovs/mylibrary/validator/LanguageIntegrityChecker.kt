package ru.otus.spring202111.avlasenkovs.mylibrary.validator

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

interface LanguageIntegrityChecker {

    fun checkBeforePost(language: Language, id: Long?)

    fun checkBeforeDelete(language: Language)

}
