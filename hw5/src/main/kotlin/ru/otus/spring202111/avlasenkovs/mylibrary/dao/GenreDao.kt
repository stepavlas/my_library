package ru.otus.spring202111.avlasenkovs.mylibrary.dao

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

interface GenreDao {

    fun findAll(): Collection<Genre>

    fun findById(id: Long): Genre?

    fun insert(genre: Genre): Genre

    fun update(id: Long, genre: Genre): Boolean

    fun delete(genre: Genre): Boolean

    fun existsByNameAndNotEqualsId(genreName: String, id: Long?): Boolean
}