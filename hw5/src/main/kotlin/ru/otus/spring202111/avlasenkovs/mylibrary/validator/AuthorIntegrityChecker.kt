package ru.otus.spring202111.avlasenkovs.mylibrary.validator

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author

interface AuthorIntegrityChecker {

    fun checkBeforePost(author: Author, id: Long?)

    fun checkBeforeDelete(author: Author)

}