package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.service.AuthorService

@ShellComponent
class AuthorSelectCommands(
    private val authorService: AuthorService
    ) {

    @ShellMethod(value = "get list of authors", key = ["list-a"])
    fun list() =
        authorService.listAuthors().map { "${it.id}: ${it.getName()}" }

    @ShellMethod(value = "get author data", key = ["get-a"])
    fun get(@ShellOption id: Long): String {
        val foundAuthor = authorService.findById(id)
        return if (foundAuthor != null) {
            "id = [${foundAuthor.id}], firstName = [${foundAuthor.firstName}], lastName = [${foundAuthor.lastName}], " +
                "birthDate = [${foundAuthor.birthDate}], aboutLink = [${foundAuthor.aboutLink}]"
        } else {
            "Author with id [$id] not found"
        }
    }
}