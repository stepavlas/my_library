package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper

import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import java.sql.ResultSet

@Component
class AuthorMapper: RowMapper<Author> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Author? {
        val id = rs.getLong("author_id")
        return if (id != 0L) {
            val firstName = rs.getString("author_first_name")
            val lastName = rs.getString("author_last_name")
            val birthDate = rs.getDate("author_birth_date")
            val aboutLink = rs.getString("author_about_link")
            Author(
                id = id,
                firstName = firstName,
                lastName = lastName,
                birthDate = birthDate?.toLocalDate(),
                aboutLink = aboutLink
            )
        } else null
    }
}