package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.service.GenreService
import ru.otus.spring202111.avlasenkovs.mylibrary.service.LanguageService

@ShellComponent
class LanguageCommands(
    private val languageService: LanguageService
) {

    @ShellMethod(value = "get list of languages", key = ["list-l"])
    fun list() = languageService.listLanguages().map { "${it.id}: ${it.language}" }

    @ShellMethod(value = "add language", key = ["add-l"])
    fun add(@ShellOption languageName: String): String {
        val language = Language(language = languageName)
        val newLanguage = languageService.addLanguage(Language(language = languageName))
        return "Language [$languageName] was added with id [${newLanguage.id}]"
    }

    @ShellMethod(value = "update language", key = ["update-l"])
    fun update(@ShellOption id: Long, languageName: String): String {
        val isUpdated = languageService.updateLanguage(id, Language(language = languageName))
        return if (isUpdated) {
            "Language with id [$id] was updated"
        } else {
            "Language with id [$id] was not found"
        }
    }

    @ShellMethod(value = "delete language", key = ["delete-l"])
    fun delete(@ShellOption id: Long): String {
        val language = languageService.deleteLanguage(id)
        return if (language != null) {
            "Language [$language] was deleted"
        } else {
            "Language with id [$id] not found"
        }
    }

}