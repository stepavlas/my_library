package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.util.CommonRegex
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.Validator
import java.lang.IllegalArgumentException

@Component
class AuthorValidator : Validator<Author> {
    override fun validateEntity(entity: Author) {
        if (entity.firstName.isNullOrBlank() || entity.firstName.length == 1) {
            throw IllegalArgumentException("Trying to insert author with null/blank/incorrect first name value")
        }
        if (entity.lastName.isNullOrBlank() || entity.lastName.length == 1) {
            throw IllegalArgumentException("Trying to insert author with null/blank/incorrect last name value")
        }
        if (entity.aboutLink != null && !entity.aboutLink.matches(CommonRegex.URL_REGEX)) {
            throw IllegalArgumentException("Trying to insert author with incorrect url value")
        }
    }
}