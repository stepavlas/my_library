package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.Availability
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellMethodAvailability
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.service.AuthorService
import ru.otus.spring202111.avlasenkovs.mylibrary.service.BookService
import ru.otus.spring202111.avlasenkovs.mylibrary.service.GenreService
import ru.otus.spring202111.avlasenkovs.mylibrary.service.LanguageService
import java.time.Year

@ShellComponent
class BookUpdateCommands(
    private val bookService: BookService,
    private val languageService: LanguageService,
    private val genreService: GenreService,
    private val authorService: AuthorService
    ) {

    companion object {
        private val NAME_NOT_SPECIFIED = "Book name not specified"
    }

    private var name: String? = null
    private var language: Language? = null
    private var year: Year? = null
    private var genre: Genre? = null
    private var author: Author? = null
    private var aboutLink: String? = null

    @ShellMethod(value = "add book", key = ["add-b"])
    @ShellMethodAvailability("checkBook")
    fun add(): String {
        val book = Book(
            name = name,
            language = language,
            year = year,
            genre = genre,
            author = author,
            aboutLink = aboutLink
        )
        val newBook = bookService.addBook(book)
        return "Book [$newBook] was added"
    }

    @ShellMethod(value = "update book", key = ["update-b"])
    @ShellMethodAvailability("checkBook")
    fun update(@ShellOption id: Long): String {
        val book = Book(
            name = name,
            language = language,
            year = year,
            genre = genre,
            author = author,
            aboutLink = aboutLink
        )
        val isUpdated = bookService.updateBook(id, book)
        return if (isUpdated) {
            "Book with id [$id] was updated"
        } else {
            "Book with id [$id[ was not found"
        }
    }

    @ShellMethod(value = "set book name", key = ["set-b-name"])
    fun setBookName(@ShellOption name: String) {
        this.name = name
    }

    @ShellMethod(value = "set book language", key = ["set-b-language"])
    fun setBookLanguage(@ShellOption languageId: Long) {
        val language = languageService.findById(languageId)
            ?: throw IllegalArgumentException("Language with id = [$languageId] not found")
        this.language = language
    }

    @ShellMethod(value = "set book year", key = ["set-b-year"])
    fun setBookYear(@ShellOption year: Int) {
        this.year = Year.of(year)
    }

    @ShellMethod(value = "set book genre", key = ["set-b-genre"])
    fun setBookGenre(@ShellOption genreId: Long) {
        val genre = genreService.findById(genreId)
            ?: throw IllegalArgumentException("Genre with id = [$genreId] not found")
        this.genre = genre
    }

    @ShellMethod(value = "set book author", key = ["set-b-author"])
    fun setBookAuthor(@ShellOption authorId: Long) {
        val author = authorService.findById(authorId)
            ?: throw IllegalArgumentException("Author with id = [$authorId] not found")
        this.author = author
    }

    @ShellMethod(value = "set book about link", key = ["set-b-about"])
    fun setBookAboutLink(@ShellOption aboutLink: String) {
        this.aboutLink = aboutLink
    }

    @ShellMethod(value = "get inserted book data", key = ["get-b-data"])
    fun getBookData() =
        "name = [$name], language = [$language], year = [$year], genre = [$genre], author = [$author], aboutLink = [$aboutLink]"

    fun checkBook(): Availability = if (name.isNullOrBlank()) {
        Availability.unavailable(NAME_NOT_SPECIFIED)
    } else {
        Availability.available()
    }

}