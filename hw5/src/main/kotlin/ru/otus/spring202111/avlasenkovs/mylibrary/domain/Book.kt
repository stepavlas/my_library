package ru.otus.spring202111.avlasenkovs.mylibrary.domain

import java.time.Year

class Book(
    val id: Long? = null,
    val name: String? = null,
    val language: Language? = null,
    val year: Year? = null,
    val genre: Genre? = null,
    val author: Author? = null,
    val aboutLink: String? = null
) {
    override fun toString(): String {
        return "Book(id=$id, name=$name)"
    }
}