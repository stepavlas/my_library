package ru.otus.spring202111.avlasenkovs.mylibrary.util

object CommonRegex {
    // Regex to check valid URL
    val URL_REGEX = Regex("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)")
}