package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.Availability
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellMethodAvailability
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.service.AuthorService
import ru.otus.spring202111.avlasenkovs.mylibrary.service.GenreService
import ru.otus.spring202111.avlasenkovs.mylibrary.service.LanguageService
import java.time.LocalDate
import java.time.format.DateTimeParseException

@ShellComponent
class AuthorUpdateCommands(
    private val authorService: AuthorService
) {
    private var firstName: String? = null
    private var lastName: String? = null
    private var birthDate: LocalDate? = null
    private var aboutLink: String? = null

    companion object {
        const val NAME_NOT_SPECIFIED = "First name or last name were not specified"
    }

    @ShellMethod(value = "add author", key = ["add-a"])
    @ShellMethodAvailability("checkAuthor")
    fun add(): String {
        val author = Author(
            firstName = firstName,
            lastName = lastName,
            birthDate = birthDate,
            aboutLink = aboutLink
        )
        val newAuthor = authorService.addAuthor(author)
        return "Author [$newAuthor] was added"
    }

    @ShellMethod(value = "update language", key = ["update-a"])
    @ShellMethodAvailability("checkAuthor")
    fun update(@ShellOption id: Long): String {
        val author = Author(
            firstName = firstName,
            lastName = lastName,
            birthDate = birthDate,
            aboutLink = aboutLink
        )
        val isUpdated = authorService.updateAuthor(id, author)
        return if (isUpdated) {
            "Author with id [$id] was updated"
        } else {
            "Author with id [$id] was not found"
        }
    }

    @ShellMethod(value = "set author name", key = ["set-a-name"])
    fun setAuthorName(@ShellOption firstName: String, @ShellOption lastName: String) {
        this.firstName = firstName
        this.lastName = lastName
    }

    @ShellMethod(value = "set author birth date in format [YYYY-MM-DD]", key = ["set-a-birth-date", "set-a-birth", "set-a-b"])
    fun setAuthorBirthDate(@ShellOption birthDate: String) {
        try {
            this.birthDate = LocalDate.parse(birthDate)
        } catch (e: DateTimeParseException) {
            throw IllegalArgumentException("Couldn't parse birth date [$birthDate]. " +
                "Please specify birth date in format [YYYY-MM-DD]")
        }
    }

    @ShellMethod(value = "set author about link", key = ["set-a-about"])
    fun setAuthorAboutLink(@ShellOption aboutLink: String) {
        this.aboutLink = aboutLink
    }

    @ShellMethod(value = "get inserted author data", key = ["get-a-data"])
    fun getAuthorData() =
        "firstName = [$firstName], lastName = [$lastName], birthDate = [$birthDate], aboutLink = [$aboutLink]"

    fun checkAuthor(): Availability =
        if (firstName.isNullOrBlank() || lastName.isNullOrBlank()) {
            Availability.unavailable(NAME_NOT_SPECIFIED)
        } else {
            Availability.available()
        }

}