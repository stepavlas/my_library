package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper

import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import java.sql.ResultSet

@Component
class LanguageMapper : RowMapper<Language> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Language? {
        val id = rs.getLong("lang_id")
        return if (id != 0L) {
            val language = rs.getString("lang_lang")
            Language(id, language)
        } else null
    }
}