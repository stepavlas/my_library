package ru.otus.spring202111.avlasenkovs.mylibrary.domain

import java.time.LocalDate

class Author(
    val id: Long? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val birthDate: LocalDate? = null,
    val aboutLink: String? = null
) {
    fun getName(): String {
        val initial = firstName?.get(0)
        return if (initial != null) {
            "$lastName $initial."
        } else {
            "$lastName"
        }
    }

    override fun toString(): String {
        return "Author(id=$id, name=${getName()})"
    }
}
