package ru.otus.spring202111.avlasenkovs.mylibrary.service

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

interface AuthorService {

    fun findById(id: Long): Author?

    fun listAuthors(): Collection<Author>

    fun addAuthor(author: Author): Author

    fun updateAuthor(id: Long, author: Author): Boolean

    fun deleteAuthor(id: Long): Author?

}
