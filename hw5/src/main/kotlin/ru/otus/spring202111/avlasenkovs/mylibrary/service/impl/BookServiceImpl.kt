package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.springframework.stereotype.Service
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.service.BookService
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.BookIntegrityChecker
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.Validator

@Service
class BookServiceImpl(
    private val bookDao: BookDao,
    private val integrityChecker: BookIntegrityChecker,
    private val bookValidator: Validator<Book>
    ) : BookService {

    override fun findBooks(searchStr: String?) =
        if (searchStr != null) {
            bookDao.findByName(searchStr).toMutableList()
        } else {
            bookDao.findAll().toList()
        }

    override fun findById(id: Long) = bookDao.findById(id)

    override fun addBook(book: Book): Book {
        bookValidator.validateEntity(book)
        integrityChecker.checkBeforePost(
            book = book,
            id = null
        )
        return bookDao.insert(book)
    }

    override fun updateBook(id: Long, book: Book): Boolean {
        bookValidator.validateEntity(book)
        integrityChecker.checkBeforePost(
            book = book,
            id = id
        )
        return bookDao.update(id, book)
    }

    override fun deleteBook(id: Long): Book? {
        val book = bookDao.findById(id) ?: return null

        val isDeleted = bookDao.delete(book)
        return if (isDeleted) book else null
    }
}