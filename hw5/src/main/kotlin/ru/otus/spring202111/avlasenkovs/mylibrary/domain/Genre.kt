package ru.otus.spring202111.avlasenkovs.mylibrary.domain

class Genre(
    val id: Long? = null,
    val genre: String? = null
) {
    override fun toString(): String {
        return "Genre(id=$id, genre=$genre)"
    }
}