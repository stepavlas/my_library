package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.AuthorDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author

@Repository
class AuthorDaoJdbc(
    private val jdbcOperations: NamedParameterJdbcOperations,
    private val authorMapper: RowMapper<Author>
) : AuthorDao {


    override fun findAll(): Collection<Author> =
        jdbcOperations.query(
            """
            select id author_id, first_name author_first_name, last_name author_last_name, 
                birth_date author_birth_date, about_link author_about_link from author
            """,
            authorMapper
        )

    override fun findById(id: Long): Author? =
        try {
            jdbcOperations.queryForObject(
                """
                select id author_id, first_name author_first_name, last_name author_last_name, 
                    birth_date author_birth_date, about_link author_about_link from author where id = :id
                """,
                mapOf("id" to id),
                authorMapper
            )
        } catch (e: EmptyResultDataAccessException) {
            null
        }


    override fun insert(author: Author): Author {
        val keyHolder = GeneratedKeyHolder()
        jdbcOperations.update(
            """
                insert into author 
                   (first_name, last_name, birth_date, about_link) 
                values 
                   (:firstName, :lastName, :birthDate, :aboutLink)
            """,
            MapSqlParameterSource(
                mapOf(
                    "firstName" to author.firstName,
                    "lastName" to author.lastName,
                    "birthDate" to author.birthDate,
                    "aboutLink" to author.aboutLink
                )
            ),
            keyHolder,
            arrayOf("id")
        )

        val id = keyHolder.key!!
        return Author(
            id = id.toLong(),
            firstName = author.firstName,
            lastName = author.lastName,
            birthDate = author.birthDate,
            aboutLink = author.aboutLink
        )
    }

    override fun update(id: Long, author: Author): Boolean {
        val numOfUpdates = jdbcOperations.update(
            """
                update author set 
                    first_name = :firstName,
                    last_name = :lastName,
                    birth_date = :birthDate,
                    about_link = :aboutLink
                where id = :id
            """,
            mapOf(
                "id" to id,
                "firstName" to author.firstName,
                "lastName" to author.lastName,
                "birthDate" to author.birthDate,
                "aboutLink" to author.aboutLink
            )
        )
        return numOfUpdates > 0
    }

    override fun delete(author: Author): Boolean {
        val numOfUpdates = jdbcOperations.update("delete from author where id = :id", mapOf("id" to author.id))
        return numOfUpdates > 0
    }

    override fun existsByNameAndNotEqualsId(firstName: String, lastName: String, id: Long?): Boolean =
        try {
            var select = "select count(id) from author where upper(first_name) = :firstName and " +
                "upper(last_name) = :lastName"
            val paramMap = mutableMapOf<String, Any>(
                "firstName" to firstName.uppercase(),
                "lastName" to lastName.uppercase()
            )
            id?.let {
                select = "$select and id <> :id"
                paramMap["id"] = id
            }

            val count = jdbcOperations.queryForObject(
                select,
                paramMap,
                Integer::class.java
            )!!
            count > 0
        } catch (e: EmptyResultDataAccessException) {
            false
        }
}