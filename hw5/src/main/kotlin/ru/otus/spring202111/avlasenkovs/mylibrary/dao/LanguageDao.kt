package ru.otus.spring202111.avlasenkovs.mylibrary.dao

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

interface LanguageDao {

    fun findAll(): Collection<Language>

    fun findById(id: Long): Language?

    fun insert(language: Language): Language

    fun update(id: Long, language: Language): Boolean

    fun delete(language: Language): Boolean

    fun existsByNameAndNotEqualsId(languageName: String, id: Long?): Boolean
}