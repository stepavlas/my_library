package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.BookIntegrityChecker

@Component
class BookIntegrityCheckerImpl(
    private val bookDao: BookDao
    ) : BookIntegrityChecker {

    override fun checkBeforePost(book: Book, id: Long?) {
        if (bookDao.existsByNameAndNotEqualsId(
                name = book.name!!,
                id = id
        )) {
            throw DataIntegrityViolationException("Book with name [${book.name}] already exists")
        }
    }
}