package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.LanguageDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.LanguageMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

@Repository
class LanguageDaoJdbc(
    private val jdbcOperations: NamedParameterJdbcOperations,
    private val languageMapper: LanguageMapper
) : LanguageDao {


    override fun findAll(): Collection<Language> =
        jdbcOperations.query("select id lang_id, lang lang_lang from lang", languageMapper)

    override fun findById(id: Long): Language? {
        return try {
            jdbcOperations.queryForObject(
                "select id lang_id, lang lang_lang from lang where id = :id",
                mapOf("id" to id),
                languageMapper
            )
        } catch (e: EmptyResultDataAccessException) {
            null
        }
    }

    override fun insert(language: Language): Language {
        val keyHolder = GeneratedKeyHolder()
        jdbcOperations.update(
            "insert into lang (lang) values (:language)",
            MapSqlParameterSource(mapOf("language" to language.language)),
            keyHolder,
            arrayOf("id")
        )

        val id = keyHolder.key!!
        return Language(id.toLong(), language.language)
    }

    override fun update(id: Long, language: Language): Boolean {
        val numOfUpdates = jdbcOperations.update("update lang set lang = :language where id = :id", mapOf("id" to id, "language" to language.language))
        return numOfUpdates > 0
    }

    override fun delete(language: Language): Boolean {
        val numOfUpdates = jdbcOperations.update("delete from lang where id = :id", mapOf("id" to language.id))
        return numOfUpdates > 0
    }

    override fun existsByNameAndNotEqualsId(languageName: String, id: Long?): Boolean =
        try {
            var select = "select count(id) from lang where upper(lang) = :languageName"
            val paramMap = mutableMapOf<String, Any>("languageName" to languageName.uppercase())
            id?.let {
                select = "$select and id <> :id"
                paramMap["id"] = id
            }

            val count = jdbcOperations.queryForObject(
                select,
                paramMap,
                Integer::class.java
            )!!
            count > 0
        } catch (e: EmptyResultDataAccessException) {
            false
        }
}