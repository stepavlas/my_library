package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.service.AuthorService

@ShellComponent
class AuthorDeleteCommands(
    private val authorService: AuthorService
    ) {

    @ShellMethod(value = "delete author", key = ["delete-a"])
    fun delete(@ShellOption id: Long): String {
        val author = authorService.deleteAuthor(id)
        return if (author != null) {
            "Author [$author] was deleted"
        } else {
            "Author with id [$id] not found"
        }
    }
}