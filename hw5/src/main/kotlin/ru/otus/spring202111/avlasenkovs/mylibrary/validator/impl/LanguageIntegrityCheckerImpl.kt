package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.LanguageDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.LanguageIntegrityChecker

@Component
class LanguageIntegrityCheckerImpl(
    private val languageDao: LanguageDao,
    private val bookDao: BookDao
    ) : LanguageIntegrityChecker {

    override fun checkBeforePost(language: Language, id: Long?) {
        if (languageDao.existsByNameAndNotEqualsId(language.language!!, id)) {
            throw DataIntegrityViolationException("Language with name [${language.language}] already exists")
        }
    }

    override fun checkBeforeDelete(language: Language) {
        if (bookDao.existsByLanguage(language)) {
            throw DataIntegrityViolationException("Can't delete language [${language} used in book")
        }
    }
}