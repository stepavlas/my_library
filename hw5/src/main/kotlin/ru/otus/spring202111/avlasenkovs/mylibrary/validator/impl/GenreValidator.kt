package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.Validator
import java.lang.IllegalArgumentException

@Component
class GenreValidator : Validator<Genre> {
    override fun validateEntity(entity: Genre) {
        if (entity.genre.isNullOrBlank()) {
            throw IllegalArgumentException("Trying to insert genre with null or blank genre value")
        }
    }
}