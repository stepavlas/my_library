package ru.otus.spring202111.avlasenkovs.mylibrary.service.impl

import org.springframework.stereotype.Service
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.AuthorDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.service.AuthorService
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.AuthorIntegrityChecker
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.Validator

@Service
class AuthorServiceImpl(
    private val authorDao: AuthorDao,
    private val integrityChecker: AuthorIntegrityChecker,
    private val validator: Validator<Author>
) : AuthorService {
    override fun listAuthors(): Collection<Author> = authorDao.findAll()

    override fun findById(id: Long): Author? = authorDao.findById(id)

    override fun addAuthor(author: Author): Author {
        validator.validateEntity(author)
        integrityChecker.checkBeforePost(
            author = author,
            id = null
        )

        return authorDao.insert(author)
    }

    override fun updateAuthor(id: Long, author: Author): Boolean {
        validator.validateEntity(author)
        integrityChecker.checkBeforePost(
            author = author,
            id = id
        )

        return authorDao.update(id, author)
    }

    override fun deleteAuthor(id: Long): Author? {
        val author = authorDao.findById(id) ?: return null
        integrityChecker.checkBeforeDelete(author)

        val isDeleted = authorDao.delete(author)
        return if (isDeleted) author else null
    }
}