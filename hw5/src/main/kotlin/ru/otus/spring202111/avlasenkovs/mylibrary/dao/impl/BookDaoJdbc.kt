package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper.BookMapper
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

@Repository
class BookDaoJdbc(
    private val jdbcOperations: NamedParameterJdbcOperations,
    private val bookMapper: BookMapper
) : BookDao {

    override fun findAll(): Collection<Book> =
        jdbcOperations.query(
            getSelectWithoutWhere(),
            bookMapper
        )

    override fun findById(id: Long): Book? =
        try {
            jdbcOperations.queryForObject(
            getSelectWithoutWhere() + " where book.id = :id",
                mapOf("id" to id),
                bookMapper
            )
        } catch (e: EmptyResultDataAccessException) {
            null
        }

    override fun findByName(name: String): Collection<Book> =
        jdbcOperations.query(
            getSelectWithoutWhere() + " where upper(book.name) LIKE :name",
            mapOf("name" to "%${name.uppercase()}%"),
            bookMapper
        )

    private fun getSelectWithoutWhere() = """
                select book.id book_id, book.name book_name, lang.id lang_id, lang.lang lang_lang, book.year book_year, genre.id genre_id, genre.genre genre_genre,
                    author.id author_id, author.first_name author_first_name, author.last_name author_last_name, 
                    author.birth_date author_birth_date, author.about_link author_about_link, book.about_link as book_about_link
                from book 
                    left join lang on book.lang_fk = lang.id
                    left join genre on book.genre_fk = genre.id
                    left join author on book.author_fk = author.id
                """

    override fun insert(book: Book): Book {
        val keyHolder = GeneratedKeyHolder()

        jdbcOperations.update(
            """
                insert into book
                   (name, lang_fk, year, genre_fk, author_fk, about_link) 
                values 
                   (:name, :languageFk, :year, :genreFk, :authorFk, :aboutLink)
            """,
            MapSqlParameterSource(
                mapOf(
                    "name" to book.name,
                    "languageFk" to book.language?.id,
                    "year" to book.year?.value,
                    "genreFk" to book.genre?.id,
                    "authorFk" to book.author?.id,
                    "aboutLink" to book.aboutLink
                )
            ),
            keyHolder,
            arrayOf("id")
        )

        val id = keyHolder.key!!
        return findById(id.toLong())!!
    }

    override fun update(id: Long, book: Book): Boolean {
        val numOfUpdates = jdbcOperations.update(
            """
                update book set 
                    name = :name,
                    lang_fk = :languageFk,
                    year = :year,
                    genre_fk = :genreFk,
                    author_fk = :authorFk,
                    about_link = :aboutLink
                where id = :id
            """,
            mapOf(
                "id" to id,
                "name" to book.name,
                "languageFk" to book.language?.id,
                "year" to book.year?.value,
                "genreFk" to book.genre?.id,
                "authorFk" to book.author?.id,
                "aboutLink" to book.aboutLink
            )
        )
        return numOfUpdates > 0
    }

    override fun delete(book: Book): Boolean {
        val numOfUpdates = jdbcOperations.update("delete from book where id = :id", mapOf("id" to book.id))
        return numOfUpdates > 0
    }

    override fun existsByGenre(genre: Genre): Boolean =
        try {
            val count = jdbcOperations.queryForObject(
                "select count(id) from book where genre_fk = :genreId",
                mapOf("genreId" to genre.id),
                Integer::class.java
            )!!
            count > 0
        } catch (e: EmptyResultDataAccessException) {
            false
        }

    override fun existsByLanguage(language: Language): Boolean =
        try {
            val count = jdbcOperations.queryForObject(
                "select count(id) from book where lang_fk = :languageId",
                mapOf("languageId" to language.id),
                Integer::class.java
            )!!
            count > 0
        } catch (e: EmptyResultDataAccessException) {
            false
        }

    override fun existsByAuthor(author: Author): Boolean =
        try {
            val count = jdbcOperations.queryForObject(
                "select count(id) from book where author_fk = :authorId",
                mapOf("authorId" to author.id),
                Integer::class.java
            )!!
            count > 0
        } catch (e: EmptyResultDataAccessException) {
            false
        }

    override fun existsByNameAndNotEqualsId(name: String, id: Long?): Boolean =
        try {
            var select = "select count(id) from book where upper(name) = :name"
            val paramMap = mutableMapOf<String, Any>(
                "name" to name.uppercase(),
            )
            id?.let {
                select = "$select and id <> :id"
                paramMap["id"] = id
            }

            val count = jdbcOperations.queryForObject(
                select,
                paramMap,
                Integer::class.java
            )!!
            count > 0
        } catch (e: EmptyResultDataAccessException) {
            false
        }
}