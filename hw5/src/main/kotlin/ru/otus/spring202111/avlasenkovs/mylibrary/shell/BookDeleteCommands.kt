package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.service.BookService

@ShellComponent
class BookDeleteCommands(
    private val bookService: BookService
    ) {

    @ShellMethod(value = "delete book", key = ["delete-b"])
    fun delete(@ShellOption id: Long): String {
        val book = bookService.deleteBook(id)
        return if (book != null) {
            "Book [$book] was deleted"
        } else {
            "Book with id = [$id] not found"
        }
    }
}