package ru.otus.spring202111.avlasenkovs.mylibrary.validator.impl

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.AuthorDao
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.BookDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.validator.AuthorIntegrityChecker

@Component
class AuthorIntegrityCheckerImpl(
    private val authorDao: AuthorDao,
    private val bookDao: BookDao
    ) : AuthorIntegrityChecker {

    override fun checkBeforePost(author: Author, id: Long?) {
        if (authorDao.existsByNameAndNotEqualsId(
                firstName = author.firstName!!,
                lastName = author.lastName!!,
                id = id
            )
        ) {
            throw DataIntegrityViolationException("Author with name [${author.firstName} ${author.lastName}] already exists")
        }
    }

    override fun checkBeforeDelete(author: Author) {
        if (bookDao.existsByAuthor(author)) {
            throw DataIntegrityViolationException("Can't delete author [${author} used by book")
        }
    }
}