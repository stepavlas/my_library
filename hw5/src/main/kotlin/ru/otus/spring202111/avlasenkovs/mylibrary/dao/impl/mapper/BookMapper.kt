package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl.mapper

import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language
import java.sql.ResultSet
import java.time.Year

@Component
class BookMapper(
    private val languageMapper: RowMapper<Language>,
    private val genreMapper: RowMapper<Genre>,
    private val authorMapper: RowMapper<Author>
) : RowMapper<Book> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Book? {
        val id = rs.getLong("book_id")
        return if (id != 0L) {
            val name = rs.getString("book_name")
            val year = rs.getInt("book_year")
            val aboutLink = rs.getString("book_about_link")
            val language = languageMapper.mapRow(rs, rowNum)
            val genre = genreMapper.mapRow(rs, rowNum)
            val author = authorMapper.mapRow(rs, rowNum)
            Book(
                id = id,
                name = name,
                language = language,
                year = Year.of(year),
                genre = genre,
                author = author,
                aboutLink = aboutLink
            )
        } else null
    }
}