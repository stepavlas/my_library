package ru.otus.spring202111.avlasenkovs.mylibrary.dao.impl

import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import ru.otus.spring202111.avlasenkovs.mylibrary.dao.GenreDao
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

@Repository
class GenreDaoJdbc(
    private val jdbcOperations: NamedParameterJdbcOperations,
    private val genreMapper: RowMapper<Genre>
) : GenreDao {

    override fun findAll(): Collection<Genre> =
        jdbcOperations.query("select id genre_id, genre genre_genre from genre", genreMapper)

    override fun findById(id: Long): Genre? {
        return try {
            jdbcOperations.queryForObject(
                "select id genre_id, genre genre_genre from genre where id = :id",
                mapOf("id" to id),
                genreMapper
            )
        } catch (e: EmptyResultDataAccessException) {
            null
        }
    }

    override fun insert(genre: Genre): Genre {
        val keyHolder = GeneratedKeyHolder()
        jdbcOperations.update(
            "insert into genre (genre) values (:genre)",
            MapSqlParameterSource(mapOf("genre" to genre.genre)),
            keyHolder,
            arrayOf("id")
        )

        val id = keyHolder.key!!
        return Genre(id.toLong(), genre.genre)
    }

    override fun update(id: Long, genre: Genre): Boolean {
        val numOfUpdates = jdbcOperations.update(
            "update genre set genre = :genre where id = :id",
            mapOf("id" to id, "genre" to genre.genre)
        )
        return numOfUpdates > 0
    }

    override fun delete(genre: Genre): Boolean {
        val numOfUpdates = jdbcOperations.update("delete from genre where id = :id", mapOf("id" to genre.id))
        return numOfUpdates > 0
    }

    override fun existsByNameAndNotEqualsId(genreName: String, id: Long?): Boolean =
        try {
            var select = "select count(id) from genre where upper(genre) = :genreName"
            val paramMap = mutableMapOf<String, Any>("genreName" to genreName.uppercase())
            id?.let {
                select = "$select and id <> :id"
                paramMap["id"] = id
            }

            val count = jdbcOperations.queryForObject(
                select,
                paramMap,
                Integer::class.java
            )!!
            count > 0
        } catch (e: EmptyResultDataAccessException) {
            false
        }
}