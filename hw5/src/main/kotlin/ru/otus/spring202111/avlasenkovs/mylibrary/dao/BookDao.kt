package ru.otus.spring202111.avlasenkovs.mylibrary.dao

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Author
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Book
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Language

interface BookDao {

    fun findAll(): Collection<Book>

    fun findById(id: Long): Book?

    fun findByName(name: String): Collection<Book>

    fun insert(book: Book): Book

    fun update(id: Long, book: Book): Boolean

    fun delete(book: Book): Boolean

    fun existsByGenre(genre: Genre): Boolean

    fun existsByLanguage(language: Language): Boolean

    fun existsByAuthor(author: Author): Boolean

    fun existsByNameAndNotEqualsId(name: String, id: Long?): Boolean
}