package ru.otus.spring202111.avlasenkovs.mylibrary.service

import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre

interface GenreService {

    fun listGenres(): Collection<Genre>

    fun addGenre(genre: Genre): Genre

    fun updateGenre(id: Long, genre: Genre): Boolean

    fun deleteGenre(id: Long): Genre?

    fun findById(genreId: Long): Genre?
}
