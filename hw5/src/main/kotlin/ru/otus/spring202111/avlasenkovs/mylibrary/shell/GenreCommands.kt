package ru.otus.spring202111.avlasenkovs.mylibrary.shell

import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import ru.otus.spring202111.avlasenkovs.mylibrary.domain.Genre
import ru.otus.spring202111.avlasenkovs.mylibrary.service.GenreService

@ShellComponent
class GenreCommands(
    private val genreService: GenreService
) {

    @ShellMethod(value = "get list of genres", key = ["list-g"])
    fun list() = genreService.listGenres().map { "${it.id}: ${it.genre}" }

    @ShellMethod(value = "add genre", key = ["add-g"])
    fun add(@ShellOption genreName: String): String {
        val genre = Genre(genre = genreName)
        val newGenre = genreService.addGenre(Genre(genre = genreName))
        return "Genre [$genreName] was added with id [${newGenre.id}]"
    }

    @ShellMethod(value = "update genre", key = ["update-g"])
    fun update(@ShellOption id: Long, genreName: String): String {
        val isUpdated = genreService.updateGenre(id, Genre(genre = genreName))
        return if (isUpdated) {
            "Genre with id [$id] was updated"
        } else {
            "Genre with id [$id] was not found"
        }
    }

    @ShellMethod(value = "delete genre", key = ["delete-g"])
    fun delete(@ShellOption id: Long): String {
        val genre = genreService.deleteGenre(id)
        return if (genre != null) {
            "Genre [$genre] was deleted"
        } else {
            "Genre with id [$id] not found"
        }
    }

}